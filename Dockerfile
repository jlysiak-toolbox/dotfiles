FROM archlinux:latest

RUN \
    pacman --noconfirm -Syu && \
    pacman --noconfirm -S stow git zsh

RUN \
    useradd -M -s /bin/zsh testzsh && \
    mkdir -p /home/testzsh && \
    chown -R testzsh:testzsh /home/testzsh

USER testzsh

WORKDIR /home/testzsh
