# Yet Another Dotfiles Repo

A set of modularized dotfiles for my machines.

## Organization

This repo may look a bit messy but here's what going on.

`modules` directory contains files for some app, etc.

## Modules
* [bash][module-bash]
* [dunst][module-dunst]
* [git][module-git]
* [i3][module-i3]
* [i3blocks][module-i3blocks]
* [i3xrocks][module-i3xrocks]
* [ipython][module-ipython]
* [mimeapps][module-mimeapps]
* [moc][module-moc]
* [nitrogen][module-nitrogen]
* [nvim][module-nvim]
* [pcmanfm][module-pcmanfm]
* [picom][module-picom]
* [screenlayouts][module-screenlayouts]
* [scripts][module-scripts]
* [shell][module-shell]
* [sxhkd][module-sxhkd]
* [tmux][module-tmux]
* [user-dirs][module-user-dirs]
* [wallpapers][module-wallpapers]
* [x][module-x]
* [zsh][module-zsh]


[module-bash]:modules/bash
[module-dunst]:modules/dunst
[module-git]:modules/git
[module-i3]:modules/i3
[module-i3blocks]:modules/i3blocks
[module-i3xrocks]:modules/i3xrocks
[module-ipython]:modules/ipython
[module-mimeapps]:modules/mimeapps
[module-moc]:modules/moc
[module-nitrogen]:modules/nitrogen
[module-nvim]:modules/nvim
[module-pcmanfm]:modules/pcmanfm
[module-picom]:modules/picom
[module-screenlayouts]:modules/screenlayouts
[module-scripts]:modules/scripts
[module-shell]:modules/shell
[module-sxhkd]:modules/sxhkd
[module-tmux]:modules/tmux
[module-user-dirs]:modules/user-dirs
[module-wallpapers]:modules/wallpapers
[module-x]:modules/x
[module-zsh]:modules/zsh
