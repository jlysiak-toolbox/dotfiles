# Copyright (C) 2021 Jacek Łysiak
# Some utils

function _activate_debug {
    [ ! -z "$_SHELL_DEBUG" ] && set -x
}

function _deactivate_debug {
    [ ! -z "$_SHELL_DEBUG" ] && set +x
}

function source_files_from_dir {
    local DIR="$1"
    local PARENT="$(dirname $DIR)"
    for FILE in `/usr/bin/ls $DIR | sort`
    do
        source $DIR/$FILE $PARENT
    done
}

function find_dirs_and_source {
    local ROOT="$1"
    local SRCFROM="$2"
    for DIR in `/usr/bin/find -L $ROOT -type d -name $SRCFROM`;
    do
        source_files_from_dir $DIR
    done
}
