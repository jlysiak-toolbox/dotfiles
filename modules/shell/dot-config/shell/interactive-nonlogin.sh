# Copyright (C) 2021 Jacek Łysiak
# Entry point for interactive non-login shells

# Get shell root dir
ROOTDIR="$(cd $(dirname $0) && pwd)"

# Load some helper functions
source $FILEDIR/utils.sh

# Non-login shell hooks
LOOKIN=$XDG_CONFIG_HOME
SRCFROM='nonlogin-hooks.d'
find_dirs_and_source $LOOKIN $SRCFROM
