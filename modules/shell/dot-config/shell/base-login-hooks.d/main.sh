# Global variables

export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="google-chrome"
export WM="i3"

CONFIG_DIR="${XDG_CONFIG_HOME}"
CACHE_DIR="${XDG_CACHE_HOME}"
DATA_DIR="${XDG_DATA_HOME}"

## TASKWARRIOR
export TASKRC="${CONFIG_DIR}/task/config"

## TIMEWARRIOR
export TIMEWARRIORDB="${CONFIG_DIR}/timew/config.cfg"

## SUDO
export SUDO_ASKPASS="$HOME/.local/bin/dmenupass"



