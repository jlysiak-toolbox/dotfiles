# XDG variables
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_RUNTIME_DIR="$HOME/.cache" # TODO Check this, pluseaudio issue?
export XDG_DOWNLOAD_DIR="$HOME/downloads"
