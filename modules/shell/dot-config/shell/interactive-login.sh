# Copyright (C) 2021 Jacek Łysiak
# Entry point for interactive login shells

# Get shell root dir
ROOTDIR="$(cd $(dirname $0) && pwd)"

# Load some helper functions
source $FILEDIR/utils.sh


# Source some base env vars
source_files_from_dir $ROOTDIR/base-login-hooks.d

# Pre hooks
LOOKIN=$XDG_CONFIG_HOME
SRCFROM='login-hooks-pre.d'
find_dirs_and_source $LOOKIN $SRCFROM

# Post hooks
LOOKIN=$XDG_CONFIG_HOME
SRCFROM='login-hooks-post.d'
find_dirs_and_source $LOOKIN $SRCFROM

# Try to run WM if not running yet
if [[ "$(tty)" == "/dev/tty1" ]]; then
        pgrep "${WM}" || startx
fi

