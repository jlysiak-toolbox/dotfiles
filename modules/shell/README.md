# SHELL

The module serves shell independent machine configs.

It should be used in conjunction with base [zsh][zsh] or [bash][bash] modules
or other shell-specific module.

## Main entry points

* `interactive-login.sh`: source from, i.e. `.profile` or `.zprofile`
* `interactive-nonlogin.sh`: source form, i.e. `.bashrc` or `.zshrc`

## Hooks

### Interactive login shell

Hooks fired once interactive login session starts.

#### Pre interactive login sourcing

At the very beginning of interactive login shell execution, bash or zsh usually
export environmental variables, etc. In modular approach, each module may want
to export its own environmental variables. However, to not introduce too much
cross-module coupling and keep only single logic for sourcing files the following
mechanism is implemented.

Once login shell starts the following files are sourced:
1. files in `shell` module's own `login-hooks-pre.d` directory
2. all files found in different `$XDG_CONFIG_HOME/**/login-hooks-pre.d`
   directories

So, each module that wants, i.e. to export variables, needs to create
`login-hooks-pre.d` directory and put there some files that will be sourced.

To simplify implementation, modules can't assume any particular order in which
files are sourced except that for each `login-hooks-pre.d` directory files
are sourced in alphabetical order.

Each file found is sourced with argument `$1` set to `login-hooks-pre.d`
parent's directory path.

#### Post interactive login sourcing

Same as in [pre interactive login sourcing][#pre-interactive-login-sourcing].
However, the algorithm looks for files in `login-hooks-post.d` directories.
Files are sourced just before starting X session.

### Interactive non-login shell

#### Interactive non-login sourcing

After logging in and starting a new terminal (or just running bash/zsh)
non-login shell pops up. With each non-login session modules may want run
some extra stuff, like define custom aliases.

The mechanism is the same as for [pre interactive login sourcing][#pre-interactive-login-sourcing].
Just use `nonlogin-hooks.d` to keep files that are sourced when non-login
session starts up.


[module-zsh]:../zsh
[module-bash]:../bash
