#!/bin/bash

PT=`env TZ=US/Pacific date`
KR=`env TZ=Europe/Berlin date`
IN=`env TZ=Asia/Kolkata date`
UT=`env TZ=Etc/GMT date`
echo “LA     $PT”
echo “UTC    $UT”
echo “Berlin $KR”
echo “India  $IN”
