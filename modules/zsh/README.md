# ZSH

ZSH module serves only entry points for `zsh` that:
1. sources appropriate general shell configs,
2. sources zsh-specific configs

Entry points:
- zsh:
    * ~/.zprofile
    * ~/.zshrc

## General info from `man` about `zsh` entry points`

For more info refer to: `man zsh`

General info:
* If `$ZDOTDIR` is unset, zsh uses `$HOME` instead.
* `/etc` prefix can be changed

1. On startup, commands are first read from `/etc/zshenv` - cannot be overridden.
    * Subsequent behaviour modified by `RCS` and `GLOBAL_RCS`.
    * It's loaded for all instances of zsh. Keep it small! Also, check `man
      zsh` if have doubts, as zsh files may be precompiled with `zshcompile`
      built-in command.
1. Source `$ZDOTDIR/.zshenv`

#### Interactive login shell
The next files loaded are as follows:
1. `/etc/zprofile`
1. `$ZDOTDIR/.zprofile`
1. `/etc/zshrc`
1. `$ZDOTDIR/.zshrc`
1. `/etc/zlogin`
1. `$ZDOTDIR/.zlogin`

On exit (via `exit` or `logout` or implicit `EOF` from terminal, but not
due to termination due to exec-ing another process. Also affected by `RCS` and
`GLOBAL_RCS`):
1. `/etc/zlogout`
2. `$ZDOTDIR/.zlogout`

#### Interactive non-login shell
After `$ZDOTDIR/.zshenv` files are loaded as follows:
1. `/etc/zshrc`
1. `$ZDOTDIR/.zshrc`

## This setup

In this setup I decided to use `$HOME` as `$ZDOTDIR`, because we cannot easily
set `$ZDOTDIR` before `$ZDOTDIR/.zshenv` is loaded.
We don't want to modify any system files.

- zsh:
    * ~/.zprofile
    * ~/.zshrc

