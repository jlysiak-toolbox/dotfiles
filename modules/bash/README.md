# BASH

Bash module serves only entry points for `bash` that:
1. sources appropriate general shell configs,
2. sources bash-specific configs

Entry points:
- bash:
    * ~/.profile
    * ~/.bashrc

## General info from `man` about `bash` entry points`

### SH
Info for Bash POSIX compatible mode.  
In Bash, if run:
    * as `sh`
    * with `--posix` flag

#### Interactive login shell
1. `/etc/profile`
1. `~/.profile`

When running Bash as `sh`, use `--noprofile` to inhibit this behaviour, so
it won't search for profile files.

#### Interactive non-login shell
Checks if `ENV` variable exists, expands it, and uses the value as a path
to a file to read and execute.

#### Non-Interactive shell
Doesn't load any files.

### BASH

For more info refer to: `man bash`

#### Interactive login shell

Invocation:
1. `/etc/profile`
1. Only first readable from:
    1. `~/.bash_profile`
    1. `~/.bash_login`
    1. `~/.profile`

Use `--noprofile` to inhibit this behaviour.

On exit:
1. `~/.bash_logout`

#### Interactive non-login shell

1. `~/.bash_rc`

* Inhibited by `--norc`
* Overridden by `--rcfile PATH`

#### Non-Interactive non-login shell

Reads `$BASH_ENV` variable and sources a file if exists and readable.
It doesn't use `$PATH` to search for a filename.

#### Non-Interactive login shell

If Bash is run non-interactively with `--login` works as [HERE](#interactive-login-shell-2)

`~/.bash_logout` is read and executed if the shell executes `exit` built-in
command.
