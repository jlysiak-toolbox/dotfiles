XDIR="${XDG_CONFIG_HOME}/x"
export XINITRC="${XDIR}/xinitrc"
export XPROFILE="${XDIR}/xprofile"
export XAUTHORITY="${XDG_RUNTIME_DIR}/Xauthority"
