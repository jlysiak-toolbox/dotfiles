# File is sources by xinit

if [ -z "$SXHKDRC" ]; then
    echo -e "ERROR: SXHKDRC env variable is not defined!"
else
    sxhkd -c $SXHKDRC &
fi
