# Module: sxhkd

`sxhkd` configuration files. However, using this module requires a fork
of `sxhkd` where `sxhkdrc` file can contain extra `include` command
that allows you to combine together different files.

## How does it work?

1. Env vars in `envs.d` are exported by `shell` module hook exporting
   environmental variables on login.
2. `login-hooks.d` files are executed when we log into machine. Scripts look for
   `sxhkd.d` directories under `$XDG_CONFIG_HOME` and combine together all found
   files into a `sxhkdrc`.
3. `xinit-hooks.d` files are executed just before `X` session is started, so we
   can run `sxhkd` daemon in background.
