import atexit
import os
import readline

# From: https://docs.python.org/3/library/readline.html#module-readline
path = None
if 'PYTHONHIST' in os.environ:
    path = os.environ['PYTHONHIST']
elif 'XDG_CACHE_HOME' in os.environ:
    path = os.path.join(os.environ['XDG_CACHE_HOME'], "python_history")

if not path:
    readline.write_history_file = lambda *args: None
    exit(1)

try:
    readline.read_history_file(path)
    h_len = readline.get_current_history_length()
except FileNotFoundError:
    open(path, 'wb').close()
    h_len = 0

def save(prev_h_len, histfile):
    new_h_len = readline.get_current_history_length()
    readline.set_history_length(1000)
    readline.append_history_file(new_h_len - prev_h_len, histfile)
atexit.register(save, h_len, path)

