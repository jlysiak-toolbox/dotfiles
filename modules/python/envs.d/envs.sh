export PYTHONDIR="${XDG_CONFIG_HOME}/python"
export PYTHONSTARTUP="${PYTHONDIR}/pythonrc.py"
export PYTHONHIST="${XDG_CACHE_HOME}/python_history"
