#!/bin/sh

set -e

[ ! -z "$DEBUG" ] && set -x

# Use stow to install files from package. Here are very useful flags:
# --dotfiles: all files with `dot-` prefix, will be installed as prefixed
#   with `.` instead, i.e. `/some/dest/dir/.X` is linked to `/my/pkg/dot-X`
# --ignore: ignore README and a special file `target`
# --no-folding: do not link whole directories, make them instead
STOW_FLAGS="--dotfiles --ignore='(README.md|target)' --no-folding"
[ ! -z "$VERBOSE" ] && STOW_FLAGS="$STOW_FLAGS -v"
[ ! -z "$DRYRUN" ] && STOW_FLAGS="$STOW_FLAGS -n"

MODULES=$@

for MODULE in $MODULES; do
    NAME=${MODULE#*/}
    MODULE_DIR=$(dirname $MODULE)

    TARGET_REPR=`cat $MODULE/target`
    TARGET=`eval "echo $TARGET_REPR"`
    echo "Installing: $MODULE @ $TARGET"
    stow --dotfiles --ignore='(README.md|target)' --no-folding -v -d $MODULE_DIR -t $TARGET $NAME
done
