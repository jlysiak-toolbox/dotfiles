#!/bin/bash

N="$(grep -n '## Modules' README.md | cut -f1 -d: )"

head -n $N README.md > README.md.new

for m in `ls modules`;
do
    echo "* [$m][module-$m]" >> README.md.new
done

printf "\n\n" >> README.md.new

for m in `ls modules`;
do
    echo "[module-$m]:modules/$m" >> README.md.new
done
mv README.md.new README.md
