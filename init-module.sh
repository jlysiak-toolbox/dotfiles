#!/bin/sh

set -x

MODULE=${1:?Module path required}
NAME=${MODULE#*/}

mkdir -p $MODULE

if [ ! -f $MODULE/README.md ]; then
    cat - > $MODULE/README.md <<EOF
# Module: $NAME
EOF

fi

if [ ! -f $MODULE/target ]; then
    cat - > $MODULE/target <<EOF
\$HOME
EOF

fi

